const  controller = {};


controller.list = (req, res) => {
    // res.send('hello word');
    req.getConnection((err, conn) => {
       conn.query('SELECT * FROM customer', (err, customers) => {
          if (err) {
              res.json(err);
          }
          res.render('customer/index', {
              data: customers
          })
       });
    });
};

controller.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        console.log(conn);
        conn.query('INSERT INTO customer set ?', [data], (err, customer) => {
            res.redirect('/');
        })
    });
};

controller.delete = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM customer WHERE id =  ?', [id], (err, customer) => {
            res.redirect('/');
        })
    });
};

controller.edit = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM customer WHERE id = ?', [id], (err, customer) => {
            res.render('customer/edit', {
                data: customer[0]
            });
        })
    });
};

controller.update = (req, res) => {
    const { id } = req.params;
    const  data  = req.body;

    req.getConnection((err, conn) => {
        conn.query('UPDATE customer SET ? WHERE id = ?', [data, id], (err, customer) => {
            res.redirect('/');
        })
    });
};

module.exports = controller;